FROM node:8.9.4

RUN useradd --user-group --create-home --shell /bin/false app

ENV HOME=/home/app

COPY package.json $HOME/dashboard/
RUN chown -R app:app $HOME

USER app
WORKDIR $HOME/dashboard

RUN npm install

CMD npm start
